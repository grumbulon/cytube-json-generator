# cytube-json-generator

A [deno](https://deno.land) module to generate custom JSON for a
[CyTube](https://github.com/calzoneman/sync) server.

## Installation

First, you'll need to
[install deno](https://deno.land/manual/getting_started/installation).

Then, you can install the module with:

```bash
deno install --allow-read --allow-write --allow-run "https://git.froth.zone/sam/cytube-json-generator/raw/branch/master/generator.ts"
```

`deno install` installs the module to DENO_INSTALL_ROOT (default `~/.deno`), so
you may need to add this to your PATH. An alternative I have is setting
`DENO_INSTALL_ROOT` to `~/.local/bin`, which I have in my PATH.

## Usage

`generator video.mp4 subs.srt` will generate a JSON file for the video and
subtitle file, and upload all three to
[f.ruina.exposed](https://f.ruina.exposed). `generator -h` will show full usage.
